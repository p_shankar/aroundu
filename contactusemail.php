<?php

$name = @trim(stripslashes($_POST['name']));
$email = @trim(stripslashes($_POST['email']));
$subject = @trim(stripslashes($_POST['subject']));
$message = @trim(stripslashes($_POST['message']));

$email_to = 'icemission@gmail.com';//replace with your email

// HTML for email to send submission details
$body = "
<br>
<p>The following information was submitted through the contact form on your website:</p>
<p><b>Name</b>: $name<br>
<b>Email</b>: $email<br>
$message
";

// Success Message
$success = "
    <div class=\"status alert alert-success\">
        <h3>Submission successful</h3>
        <p>Thank you for contacting us. We will get in touch with you as early as possible.</p>
    </div>
";

// Error Message
$error = "
    <div class=\"status alert alert-danger\">
        <h3>Submission unsuccessful</h3>
        <p>Form submission failed. Please try again...</p>
    </div>
";

$headers = "From: $name <$email> \r\n";
$headers .= "Reply-To: $email \r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$message = "<html><body>$body</body></html>";

if (mail($email_to, $subject, $message, $headers)) {
    echo "$success"; // success
} else {
    echo "$error"; // failure
}

?>